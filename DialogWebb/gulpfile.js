﻿/// <binding Clean='clean' />
"use strict";

var gulp = require("gulp"),
    rimraf = require("rimraf"),
    concat = require("gulp-concat"),
    cssmin = require("gulp-cssmin"),
    uglify = require("gulp-uglify"),
    less = require("gulp-less"),
    watch = require("gulp-watch"),
    rename = require("gulp-rename"),
    typescript = require("gulp-tsc");

var paths = {
    webroot: "./wwwroot/"
};
paths.less = paths.webroot + "less/**/*.less",
paths.ts = paths.webroot + "ts/**/*.ts",
paths.cssFolder = paths.webroot + "/css",
paths.jsFolder = paths.webroot + "/js",
paths.js = paths.webroot + "js/**/*.js";
paths.minJs = paths.webroot + "js/**/*.min.js";
paths.css = paths.webroot + "css/**/*.css";
paths.minCss = paths.webroot + "css/**/*.min.css";
paths.concatJsDest = paths.webroot + "js/site.min.js";
paths.concatCssDest = paths.webroot + "css/style.min.css";

gulp.task("clean:js", function (cb) {
    console.log("Clean up the jsmess")
    rimraf(paths.concatJsDest, cb);
});

gulp.task("clean:css", function (cb) {
    console.log("Clean up the cssmess")
    rimraf(paths.concatCssDest, cb);
});

gulp.task("min:js", function () {
    console.log("Starting min js!");
    // return gulp.src([paths.js, "!" + paths.minJs], { base: "." })
    return gulp.src([paths.js], { base: "." })
        .pipe(concat(paths.concatJsDest))
        .pipe(uglify())
        .pipe(gulp.dest("."));
});
gulp.task("min:css", function () {
    console.log("Starting min css!");
    return gulp.src(paths.css)
       .pipe(concat(paths.concatCssDest))
       .pipe(cssmin())
       .pipe(gulp.dest("."));
});
gulp.task("compile-Less", function () {
    console.log("Starting compile less!");
    return gulp.src(paths.less)
    .pipe(less(paths.less))
    .pipe(gulp.dest(paths.cssFolder));
});
gulp.task("compile-ts", function () {
    gulp.src(paths.ts)
      .pipe(typescript())
      .pipe(gulp.dest(paths.jsFolder))
});
gulp.task("watch", function () {
    gulp.watch([paths.less], ["compile-Less", "clean:css", "min:css"]);
    gulp.watch([paths.css], ["clean:css", "min:css"]);
    gulp.watch([paths.js], ["clean:js", "min:js"]);
    gulp.watch([paths.ts], ["compile-ts"]);
});

gulp.task("min", ["min:js", "min:css"]);
gulp.task("clean", ["clean:js", "clean:css"]);




