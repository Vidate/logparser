var DialogWeb;
(function (DialogWeb) {
    var Dialog = (function () {
        function Dialog(value, count) {
            this.value = value;
            this.count = count;
        }
        return Dialog;
    }());
})(DialogWeb || (DialogWeb = {}));
