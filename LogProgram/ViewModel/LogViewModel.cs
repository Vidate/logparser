﻿using Commander;
using LogProgram.Models;
using LogProgram.Services;
using LogProgram.Services.JsonFileService;
using LogProgram.Services.LogerService;
using Microsoft.Win32;
using Microsoft.WindowsAPICodePack.Dialogs;
using Newtonsoft.Json;
using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;

namespace LogProgram.ViewModel
{
    [ImplementPropertyChanged]
    public class LogViewModel
    {
        private readonly ILoggerService _logerService;
        private readonly INorlmalizerService _norlmalizerService;
        private readonly IJSONFileService _JSONFileService;
        private LogModel _selectedItem;
        public string Title { get; set; }
        public GridLength FirstGridHeight { get; set; }
        public GridLength SecondGridHeight { get; set; }

        public string SelectedPath { get; set; }
        public string PathToFolder { get; set; }
        public string Path { get; set; }
        public bool LoadingLogs { get; set; }
        public LogModel SelectedItem
        {
            get
            {
                return this._selectedItem;
            }
            set
            {
                SelectedItemOriginVersion = _logerService.GenerateSingleLog(value.Log);
                this._selectedItem = value;
            }
        }
        public string SelectedItemOriginVersion { get; set; }

        public ObservableCollection<TreeModel> TreeLogCollection { get; set; }
        public ObservableCollection<TreeModel> LogsDefault { get; set; }
        public string SavingResult { get; private set; }

        [OnCommand("OpenFileDialog")]
        private async void OpenFileDialog()
        {
            await Task.Run(() => CreateCollections());
            CommonOpenFileDialog dialog = new CommonOpenFileDialog();
            dialog.InitialDirectory = _JSONFileService.LoadLastOpenFilePath();
            dialog.IsFolderPicker = true;
            if (dialog.ShowDialog() == CommonFileDialogResult.Ok)
            {
                _JSONFileService.SaveSelectedPath(dialog.FileName);
                PathToFolder = dialog.FileName;
                if (!String.IsNullOrEmpty(PathToFolder))
                {
                    Title = PathToFolder;
                    _JSONFileService.SaveSelectedPath(PathToFolder);
                    string[] pathsToFile = Directory.GetFiles(PathToFolder, "*.Log");
                    LoadingLogs = true;
                    LogsDefault = await Task.Run(() => _logerService.GetLogs(pathsToFile));
                    TreeLogCollection = await Task.Run(() => _logerService.GetLogs(pathsToFile));
                    TreeLogCollection = _norlmalizerService.NormalizeTreeLogCollection(TreeLogCollection);
                    TreeLogCollection = _norlmalizerService.SortByTitle(TreeLogCollection);
                    //Titles = new ObservableCollection<string>(TreeLogCollection.Select(x => x.Title));
                    LoadingLogs = false;
                    //SelectedItem = Logs.First();
                }
            }
            //OpenFileDialog openFileDialog = new Microsoft.Win32.OpenFileDialog()
            //{
            //    Filter = "Log|*.Log|All files (*.*)|*.*",
            //    Title = "OpenFile Dialog",
            //    RestoreDirectory = true,
            //    Multiselect = true
            //};
            //if (openFileDialog.ShowDialog() == true)
            //{
            //    //Path = openFileDialog.FileName;
            //    //Paths = openFileDialog.FileNames;
            //}
        }

        [OnCommand("SaveSplitterPostion")]
        private void SaveSplitterPostion(Grid grid)
        {
            var secondGridDef = grid.RowDefinitions[1];
            var lastGridDef = grid.RowDefinitions[2];
            _JSONFileService.SaveFirstGridSplitterPostion(secondGridDef.Height);
            _JSONFileService.SaveSecondGridSplitterPostion(lastGridDef.Height);
        }
        [OnCommand("SetSelectedItem")]
        private void SetSelectedItem(LogModel logModel)
        {
            var searchResult = TreeLogCollection.Where(x => x.LogContainer.Logs.Any(y => y.Id == logModel.Id)).FirstOrDefault();
            SelectedItem = searchResult.LogContainer.Logs.Where(x => x.Id == logModel.Id).FirstOrDefault();
        }
        [OnCommand("CheckAllChildren")]
        private void CheckAllChildren(string title)
        {
            TreeLogCollection.Where(x => x.Title == title).Select(item =>
            {
                item.LogContainer.Logs.Select(log =>
                {
                    log.IsChecked = item.IsChecked;
                    return log;
                }).ToList();
                return item;
            }).ToList();
        }
        [OnCommandCanExecute("SaveLogs")]
        private bool CanSaveLogs()
        {
            return TreeLogCollection.Any(x => x.LogContainer.Logs.Any(y => y.IsChecked == true));
        }
        [OnCommand("SaveLogs")]
        private async void SaveLogs()
        {
            LoadingLogs = true;
            try
            {
                var pathToSave = PathToFolder + @"\FilterdLogs\MHDIAL_Server.log";
                await Task.Run(() => this._logerService.SaveFile(TreeLogCollection, pathToSave));
                SavingResult = "Saved!";

            }
            catch (Exception)
            {
                SavingResult = "Something goes wrong with saving";
            }
            LoadingLogs = false;
        }
        [OnCommandCanExecute("AddLog")]
        private bool CanAddLog()
        {
            return SelectedItem != null;
        }
        [OnCommandCanExecute("RemoveLog")]
        private bool CanRemoveLog()
        {
            return SelectedItem != null;
        }
        public LogViewModel(ILoggerService logerService, INorlmalizerService norlmalizerService, IJSONFileService JSONFileService)
        {
            this._norlmalizerService = norlmalizerService;
            this._logerService = logerService;
            this._JSONFileService = JSONFileService;
            CreateCollections();
            FirstGridHeight = _JSONFileService.LoadFirstGridSplitterPostion();
            SecondGridHeight = _JSONFileService.LoadSecondGridSplitterPostion();
        }
        private void CreateCollections()
        {
            this.LogsDefault = new ObservableCollection<TreeModel>();
            this.TreeLogCollection = new ObservableCollection<TreeModel>();
        }

    }
}
