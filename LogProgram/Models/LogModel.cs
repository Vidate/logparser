﻿using PropertyChanged;
using System;
namespace LogProgram.Models
{
    [ImplementPropertyChanged]
    public class LogModel
    {
        public bool IsChecked { get; set; }
        public Guid Id { get; private set; }
        public Log Log { get; set; }
        public LogModel()
        {
            Id = Guid.NewGuid();
        }
    }
}