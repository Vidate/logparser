﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LogProgram.Models
{
    public class LogParser : ILogParser
    {
        public LogParser()
        {
        }

        public LogContainer ReadLogsFromFile(string file)
        {
            var logs = SeperateLogs(file);
            return SeperateLogsContent(logs);
        }

        private LogContainer SeperateLogsContent(string[] logs)
        {
            var result = new LogContainer();
            foreach (var logItem in logs)
            {
                var log = new LogModel();
                var parts = SplitLogToMainParts(logItem);
                if (CanBeValidException(parts))
                {
                    var lines = SeperateToLines(parts);
                    log.Log = new Log();
                    log.Log.ExceptionTitle = GetTitleException(lines);
                    log.Log.InnerExceptions = GetExceptionsDescription(lines);
                    log.Log.Time = GetTimeOfException(parts);
                    log.Log.Rest = GetRestDescription(parts);
                    result.Logs.Add(log);
                }
            }
            return result;
        }
        private DateTime GetTimeOfException(string[] parts)
        {

            try
            {
                return DateTime.ParseExact(SeperateToLines(parts[1])[0], "dd-MM-yyyy HH:mm:ss",
                    CultureInfo.InvariantCulture);
            }
            catch
            {
                try
                {
                    return DateTime.ParseExact(SeperateToLines(parts[1])[0], "yyyy-MM-dd HH:mm:ss",
                        CultureInfo.InvariantCulture);
                }
                catch
                {
                    try
                    {
                        return DateTime.ParseExact(SeperateToLines(parts[1])[0], "dd/MM/yyyy HH:mm:ss",
                            CultureInfo.InvariantCulture);
                    }
                    catch
                    {
                        return DateTime.ParseExact(SeperateToLines(parts[1])[0], "d-M-yyyy HH:mm:ss",
                            CultureInfo.InvariantCulture);
                    }
                }
            }
        }
        private string GetRestDescription(string[] parts)
        {
            return "Time: " + parts[1];
        }
        private List<ExceptionContainer> GetExceptionsDescription(string[] lines)
        {
            var result = new List<ExceptionContainer>();
            if (lines.Length > 1)
            {
                int startAt = 1;
                if (lines.Length == 2)
                {
                    result.Add(new ExceptionContainer()
                    {
                        Key = "Description",
                        Value = CleanStringFromEmptySpace(lines[1])
                    });
                    startAt = 2;
                }
                result.AddRange(GetKeyValuePairFromLine(lines, startAt));
            }
            else
            {
                result.Add(new ExceptionContainer()
                {
                        Key = "Description",
                        Value = "Empty"
                });
            }
            return result;
        }
        private List<ExceptionContainer> GetKeyValuePairFromLine(string[] lines, int firstLineIndex)
        {
            var result = new List<ExceptionContainer>();
            for (int i = firstLineIndex; i < lines.Length; i++)
            {
                var seperateLine = SeperateLineToKeyValue(lines[i]);
                if (seperateLine.Length > 1)
                {
                    result.Add(new ExceptionContainer()
                    {
                        Key = CleanStringFromEmptySpace(seperateLine[0]),
                        Value = CleanStringFromEmptySpace(seperateLine[1])
                    });
                }
                else
                {
                    var exc = new ExceptionContainer();
                    exc.Key = CleanStringFromEmptySpace(seperateLine[0]);
                    for (int j = i + 1; j < lines.Length; j++)
                    {
                        exc.Value += CleanStringFromEmptySpace(lines[j]);
                    }
                    result.Add(exc);
                    break;
                }
            }
            return result;
        }
        private string[] SeperateLineToKeyValue(string line)
        {
            return Split(line, ": ");
        }
        private string GetTitleException(string[] parts)
        {
            //check safety (array lenght, is null etc)
            return CleanStringFromEmptySpace(parts[0]);
        }
        private string CleanStringFromEmptySpace(string stringToClean)
        {
            string resutl = null;
            resutl = Regex.Replace(stringToClean, "\r|\n|\t", " ");
            resutl = Regex.Replace(resutl, " {2,}", " ");
            return resutl;
        }
        private string[] SeperateToLines(string[] item)
        {
            return Split(item[0], "\r\n");
        }
        private string[] SeperateToLines(string item)
        {
            return Split(item, "\r\n");
        }
        private bool CanBeValidException(string[] exceptionContent)
        {
            if (exceptionContent.Length <= 3)
            {
                return exceptionContent[0] != "\r\n" && exceptionContent[1] != "\r\n";
            }
            return false;

        }
        private string[] SplitLogToMainParts(string exception)
        {
            var resutl = Split(exception, "Time: ");
            return resutl;
        }
        private string[] SeperateLogs(string file)
        {
            return Split(file, "Exception:");
        }

        private string[] Split(string content, string seperator)
        {
            return content.Split(new string[] { seperator }, StringSplitOptions.RemoveEmptyEntries);
        }

        
    }
}
