﻿using Autofac;
using LogProgram.Services;
using LogProgram.Services.JsonFileService;
using LogProgram.Services.LogerService;
using LogProgram.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogProgram.Models
{
    public static class Resolver
    {

        public static IContainer BaseContainer { get; private set; }

        public static void Build()
        {
            if (BaseContainer == null)
            {
                var builder = new ContainerBuilder();
                builder.RegisterType<LogerService>().As<ILoggerService>();
                builder.RegisterType<Log>();
                builder.RegisterType<LogViewModel>();
                builder.RegisterType<ExceptionContainer>();
                builder.RegisterType<JSONFileService>().As<IJSONFileService>();
                builder.RegisterType<NorlmalizerService>().As<INorlmalizerService>();
                BaseContainer = builder.Build();
            }
        }

        public static TService Resolve<TService>()
        {
            return BaseContainer.Resolve<TService>();
        }
    }
}