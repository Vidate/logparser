﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogProgram.Models
{
    [ImplementPropertyChanged]
    public class TreeModel
    {
        public bool IsChecked { get; set; }
        public string Title { get; set; }
        public LogContainer LogContainer { get; set; }
        public TreeModel()
        {
            LogContainer = new LogContainer();
        }
    }
}
