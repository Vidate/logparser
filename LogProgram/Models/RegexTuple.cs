﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogProgram.Models
{
    public static class RegexTuple
    {
        //public string Expression { get; set; }
        //public string ToReplace { get; set; }
        private static List<Tuple<string, string>> _toReturn = new List<Tuple<string, string>>();
        public static List<Tuple<string, string>> Expressions
        {
            get
            {
                return _toReturn;
            }
            set
            {
                foreach (var item in value)
                {
                    if (!_toReturn.Any(x => x.Item1 == item.Item1))
                    {
                        _toReturn.Add(item);
                    }
                }
                //_toReturn = temp;
            }
        }
    }
}
