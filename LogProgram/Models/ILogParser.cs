﻿namespace LogProgram.Models
{
    public interface ILogParser
    {
        LogContainer ReadLogsFromFile(string directory);
    }
}