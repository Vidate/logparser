﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogProgram.Models
{
    [ImplementPropertyChanged]
    public class Log
    {
        public string Rest { get; set; }
        public DateTime Time { get; set; }
        public string ExceptionTitle { get; set; }
        public ICollection<ExceptionContainer> InnerExceptions { get; set; }
        public Log()
        {
            InnerExceptions = new List<ExceptionContainer>();
        }
    }
}
