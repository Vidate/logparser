﻿using PropertyChanged;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogProgram.Models
{
    [ImplementPropertyChanged]
    public class LogContainer
    {
        public ICollection<LogModel> Logs { get; set; }
        public LogContainer()
        {
            Logs = new List<LogModel>();
        }
    }
}
