﻿using LogProgram.Models;
using LogProgram.Services;
using LogProgram.Services.LogerService;
using LogProgram.ViewModel;
using MahApps.Metro.Controls;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Configuration;
using Newtonsoft.Json;
using System.IO;
using Newtonsoft.Json.Linq;
using LogProgram.Services.JsonFileService;

namespace LogProgram
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : MetroWindow
    {
        public MainWindow()
        {
            string startupPath = Environment.CurrentDirectory;
            string json = File.ReadAllText(startupPath + "\\reqexConfing.txt");
            List<Tuple<string, string>> stuff = JsonConvert.DeserializeObject<List<Tuple<string, string>>>(json);
            RegexTuple.Expressions = stuff;
            Resolver.Build();
            this.DataContext = new LogViewModel(Resolver.Resolve<ILoggerService>(), Resolver.Resolve<INorlmalizerService>(),Resolver.Resolve<IJSONFileService>());
            InitializeComponent();
        }

    }
}
