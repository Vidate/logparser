﻿using LogProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LogProgram.Services.LogerService
{
    public interface ILoggerService
    {
        void SaveFile(ICollection<TreeModel> treeLogCollection, string directiory);
        ObservableCollection<TreeModel> GetLogs(string path);
        ObservableCollection<TreeModel> GetLogs(string[] paths);
        string GenerateSingleLog(Log item);
    }
}
