﻿using LogProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LogProgram.Services.LogerService
{
    public class LogerService : ILoggerService
    {
        public LogerService()
        {

        }
        public ObservableCollection<TreeModel> GetLogs(string[] paths)
        {
            var files = ReadFilesFromPaths(paths);
            var result = new ObservableCollection<TreeModel>();
            result = GetLogsFromMultipleFiles(files);
            return result;
        }

        private ObservableCollection<TreeModel> GetLogsFromMultipleFiles(string[] files)
        {
            var result = new ObservableCollection<TreeModel>();
            var logContainer = new LogContainer();
            foreach (var file in files)
            {
                AddLogsToLogContainer(logContainer, file);
            }
            return GroupLogContainerByTitle(logContainer);
        }

        private void AddLogsToLogContainer(LogContainer logContainer, string file)
        {
            var loadLogs = ReadLogsFromFile(file);
            foreach (var item in loadLogs.Logs)
            {
                logContainer.Logs.Add(item);
            }
        }

        private LogContainer ReadLogsFromFile(string item)
        {
            var logParser = new LogParser();
            return logParser.ReadLogsFromFile(item);
        }

        private string[] ReadFilesFromPaths(string[] paths)
        {
            return paths.Select(x => File.ReadAllText(x)).ToArray();
        }

        public ObservableCollection<TreeModel> GetLogs(string path)
        {
            var file = File.ReadAllText(path);
            ObservableCollection<TreeModel> result = new ObservableCollection<TreeModel>();
            var logContainer = ReadLogsFromFile(file);
            result = GroupLogContainerByTitle(logContainer);

            SortLogContainerLogsByDate(result);

            return result;

            //var result = new ObservableCollection<Log>();
            //var logs = Split(_file, "Exception:");
            //foreach (var item in logs)
            //{
            //    var container = new Log();
            //    var log = Split(item, "Time: ");
            //    if (log[0] != "\r\n" && log[1] != "\r\n")
            //    {
            //        var pice = Split(log[0], "\r\n");
            //        container.ExceptionTitle = Regex.Replace(pice[0].Trim(new char[] { '\r', '\n', '\t' }), " {2,}", " ");
            //        if (pice.Length > 1)
            //        {
            //            int startAt = 1;
            //            if (pice.Length == 2)
            //            {
            //                container.InnerExceptions.Add(new ExceptionContainer()
            //                {
            //                    Key = "Description",
            //                    Value = Regex.Replace(pice[1].Trim(new char[] { '\r', '\n', '\t' }), " {2,}", " ")

            //                });
            //                startAt = 2;
            //            }
            //            for (int i = startAt; i < pice.Length; i++)
            //            {
            //                var atom = Split(pice[i], ": ");
            //                if (atom.Length > 1)
            //                {
            //                    container.InnerExceptions.Add(new ExceptionContainer()
            //                    {
            //                        Key = Regex.Replace(atom[0].Trim(new char[] { '\r', '\n', '\t' }), " {2,}", " "),
            //                        Value = Regex.Replace(atom[1].Trim(new char[] { '\r', '\n', '\t' }), " {2,}", " ")
            //                    });
            //                }
            //                else
            //                {
            //                    var exc = new ExceptionContainer();
            //                    exc.Key = Regex.Replace(atom[0].Trim(new char[] { '\r', '\n', '\t' }), " {2,}", " ");
            //                    for (int j = i + 1; j < pice.Length; j++)
            //                    {
            //                        exc.Value += Regex.Replace(pice[j].Trim(new char[] { '\r', '\n', '\t' }), " {2,}", " ");
            //                    }
            //                    break;
            //                }
            //            }
            //        }
            //        else
            //        {
            //            container.InnerExceptions.Add(new ExceptionContainer()
            //            {
            //                Key = "Description",
            //                Value = "Empty"
            //            });
            //        }
            //        container.Rest = "Time:" + Regex.Replace(log[1], " {2,}", " ");
            //        result.Add(container);
            //    }
            //}
        }

        private void SortLogContainerLogsByDate(ObservableCollection<TreeModel> result)
        {
            result.Select(x =>
            {
                return x.LogContainer.Logs.OrderBy(y => y.Log.Time);
            }).ToList();
        }

        private ObservableCollection<TreeModel> GroupLogContainerByTitle(LogContainer logContainer)
        {
            var result = new ObservableCollection<TreeModel>();
            var groupedLogs = GroupLogsByTitle(logContainer);
            foreach (var group in groupedLogs)
            {
                if (CheckCollectionNotContainTitle(result, group.Key))
                {
                    result.Add(new TreeModel() { Title = group.Key, LogContainer = new LogContainer() { Logs = new ObservableCollection<LogModel>(group) } });
                }
                else
                {
                    AddLogsToTheirGroups(result, group);
                }
            }
            return result;
        }

        private void AddLogsToTheirGroups(ObservableCollection<TreeModel> result, IGrouping<string, LogModel> group)
        {
            foreach (var log in group)
            {
                result.Where(x => x.Title == group.Key).FirstOrDefault().LogContainer.Logs.Add(log);
            }
        }

        private bool CheckCollectionNotContainTitle(ObservableCollection<TreeModel> result, string key)
        {
            return !result.Any(x => x.Title == key);
        }

        private IEnumerable<IGrouping<string, LogModel>> GroupLogsByTitle(LogContainer logContainer)
        {
            return logContainer.Logs.GroupBy(x => x.Log.ExceptionTitle);
        }

        private string[] Split(string content, string seperator)
        {
            return content.Split(new string[] { seperator }, StringSplitOptions.RemoveEmptyEntries);
        }


        public void SaveFile(ICollection<TreeModel> treeLogCollection, string directiory)
        {

            var logsToSave = SelectLogsToSave(treeLogCollection);
            if (logsToSave.Count >= 1)
            {
                string fileToSave = CreateFileFromLogs(logsToSave);
                FileInfo file = new FileInfo(directiory);
                file.Directory.Create();
                File.WriteAllText(file.FullName, fileToSave);

            }
        }

        private string CreateFileFromLogs(ICollection<Log> logsToSave)
        {
            var result = "";
            foreach (var item in logsToSave)
            {
                result += GenerateSingleLog(item) + "\r\n";
            }
            return result;
        }

        public string GenerateSingleLog(Log item)
        {
            var result = "";
            result += $"Exception: {item.ExceptionTitle}\r\n";
            result += GenerateExceptionDescription(item.InnerExceptions);
            result += item.Rest;
            return result;
        }

        private string GenerateExceptionDescription(ICollection<ExceptionContainer> innerExceptions)
        {
            var result = "";
            foreach (var item in innerExceptions)
            {
                result += $"{item.Key}: {item.Value}\r\n";
            }
            return result;
        }

        private ICollection<Log> SelectLogsToSave(ICollection<TreeModel> treeLogCollection)
        {
            var nestedLogCollection = treeLogCollection.Select(x => x.LogContainer.Logs.Where(y => y.IsChecked).Select(item =>
            {
                return item.Log;
            }));
            var result = GetLogsCollection(nestedLogCollection);
            return result;
        }

        private ICollection<Log> GetLogsCollection(IEnumerable<IEnumerable<Log>> nestedLogCollection)
        {
            var result = new List<Log>();
            foreach (var item in nestedLogCollection)
            {
                result.AddRange(item);
            }
            return result;
        }
    }
}
