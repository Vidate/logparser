﻿using LogProgram.Models;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace LogProgram.Services
{
    public class NorlmalizerService : INorlmalizerService
    {
        public NorlmalizerService()
        {

        }
        public string NormalizeString(string stringToClean)
        {
            foreach (var expression in RegexTuple.Expressions)
            {
                Match result = new Regex(expression.Item1).Match(stringToClean);
                if (result.Success)
                {
                    var a = Regex.Replace(stringToClean, expression.Item1, expression.Item2);
                    return Regex.Replace(stringToClean, expression.Item1, expression.Item2);
                }
            }
            return stringToClean;
        }

        public ObservableCollection<TreeModel> NormalizeTreeLogCollection(ObservableCollection<TreeModel> treeLogCollection)
        {
            foreach (var item in treeLogCollection)
            {
                item.Title = NormalizeString(item.Title);
            }
            return treeLogCollection;
        }

        public ObservableCollection<TreeModel> SortByTitle(ObservableCollection<TreeModel> treeLogCollection)
        {
            var result = new ObservableCollection<TreeModel>();

            foreach (var item in treeLogCollection.OrderBy(x=>x.Title))
            {
                if (FindIfTitleNotExist(result, item.Title))
                {
                    result.Add(item);
                }
                else
                {
                    result = AddLogsToTheirGroup(result, item);
                }
            }
            return result;
        }

        private ObservableCollection<TreeModel> AddLogsToTheirGroup(ObservableCollection<TreeModel> result, TreeModel item)
        {
            foreach (var log in item.LogContainer.Logs)
            {
                result.Where(x => x.Title == item.Title).FirstOrDefault().LogContainer.Logs.Add(log);
            }
            return result;
        }

        private bool FindIfTitleNotExist(ObservableCollection<TreeModel> result, string title)
        {
            return !result.Any(x => x.Title == title);
        }
    }
}
