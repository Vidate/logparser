﻿using LogProgram.Models;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace LogProgram.Services
{
    public interface INorlmalizerService
    {
        string NormalizeString(string stringToClean);
        ObservableCollection<TreeModel> NormalizeTreeLogCollection(ObservableCollection<TreeModel> treeLogCollection);
        ObservableCollection<TreeModel> SortByTitle(ObservableCollection<TreeModel> treeLogCollection);
    }
}