﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LogProgram.Services.JsonFileService
{
    public class JSONFileService : IJSONFileService
    {
        public GridLength LoadFirstGridSplitterPostion()
        {
            string startupPath = Environment.CurrentDirectory;
            string json = File.ReadAllText(startupPath + "\\firstPositon.txt");
            return JsonConvert.DeserializeObject<GridLength>(json);
        }

        public string LoadLastOpenFilePath()
        {
            string startupPath = Environment.CurrentDirectory;
            string json = File.ReadAllText(startupPath + "\\selectedPath.txt");
            return JsonConvert.DeserializeObject<string>(json);
        }

        public List<Tuple<string, string>> LoadRegExConfiguration()
        {
            throw new NotImplementedException();
        }

        public GridLength LoadSecondGridSplitterPostion()
        {
            string startupPath = Environment.CurrentDirectory;
            string json = File.ReadAllText(startupPath + "\\secondPositon.txt");
            return JsonConvert.DeserializeObject<GridLength>(json);
        }

        public void SaveFirstGridSplitterPostion(GridLength postion)
        {
            string startupPath = Environment.CurrentDirectory;
            var json = JsonConvert.SerializeObject(postion);
            File.WriteAllText(startupPath + "\\firstPositon.txt", json);
        }

        public void SaveSecondGridSplitterPostion(GridLength postion)
        {
            string startupPath = Environment.CurrentDirectory;
            var json2 = JsonConvert.SerializeObject(postion);
            File.WriteAllText(startupPath + "\\secondPositon.txt", json2);
        }

        public void SaveSelectedPath(string path)
        {
            string startupPath = Environment.CurrentDirectory;
            var json2 = JsonConvert.SerializeObject(path);
            File.WriteAllText(startupPath + "\\selectedPath.txt", json2);
        }
    }
}
