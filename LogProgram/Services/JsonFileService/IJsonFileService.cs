﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace LogProgram.Services.JsonFileService
{
    public interface IJSONFileService
    {
        GridLength LoadFirstGridSplitterPostion();
        GridLength LoadSecondGridSplitterPostion();
        void SaveFirstGridSplitterPostion(GridLength postion);
        void SaveSecondGridSplitterPostion(GridLength postion);
        List<Tuple<string,string>> LoadRegExConfiguration();
        string LoadLastOpenFilePath();
        void SaveSelectedPath(string path);
    }
}
